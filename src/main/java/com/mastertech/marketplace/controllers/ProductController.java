package com.mastertech.marketplace.controllers;

import java.net.URI;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mastertech.marketplace.helpers.URIBuilder;
import com.mastertech.marketplace.models.Product;
import com.mastertech.marketplace.models.User;
import com.mastertech.marketplace.repositories.ProductRepository;
import com.mastertech.marketplace.repositories.UserRepository;

@RestController
@RequestMapping("/products")
public class ProductController {
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<?> getAll() {
		return productRepository.findAll();
	}
	
	@RequestMapping(path="/users/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> getAllByOwner(@PathVariable int id) {
		Optional<User> userQuery = userRepository.findById(id);
		
		if(userQuery.isPresent()) {
			User user = userQuery.get();
			Iterable<Product> products = productRepository.findAllByOwner(user);
			
			return ResponseEntity.ok(products);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody Product product) {
		Product savedProduct = productRepository.save(product);
		
		URI uri = URIBuilder.fromString("/user/" + savedProduct.getId());
		
		return ResponseEntity.created(uri).build();
	}
	
	@RequestMapping(path="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<?> update(@PathVariable int id, @RequestBody Product updatedProduct) {
		Optional<Product> productQuery = productRepository.findById(id);
		
		if(productQuery.isPresent()) {
			Product product = productQuery.get();
			
			product.setPrice(updatedProduct.getPrice());
			product.setDescription(updatedProduct.getDescription());
			product.setTitle(updatedProduct.getTitle());
			
			productRepository.save(product);
			
			return ResponseEntity.ok(product);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@RequestMapping(path="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable int id) {
		Optional<Product> productQuery = productRepository.findById(id);
		
		if(productQuery.isPresent()) {
			productRepository.delete(productQuery.get());
			return ResponseEntity.ok().build();
		}
		
		return ResponseEntity.notFound().build();
	}
}	